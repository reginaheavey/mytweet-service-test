package app.test;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import javax.sound.midi.MidiDevice.Info;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import app.main.TweetAPI;
import app.models.Tweet;

public class TweetTest 
{
	
	final private static String TWEETER_ID = "888";
	
	static Tweet tweetArray[] = 
		{ 
			new Tweet("junit-test-tweet1"), 
			new Tweet("junit-test-tweet2"),
			new Tweet("junit-test-tweet3"), 
			new Tweet("junit-test-tweet4"), 
			new Tweet("junit-test-tweet5"), 
			};

	static List<Tweet> tweetList = new ArrayList<>();

	private static TweetAPI service = new TweetAPI();

	@BeforeClass
	public static void setup() throws Exception 
	{
		for (Tweet tweet : tweetArray) 
		{
			Tweet returned = service.createTweet(TWEETER_ID, tweet);
			tweetList.add(returned);
		}
		System.out.println(tweetList);
		
	}

	
	@AfterClass
	public static void teardown() throws Exception 
	{
		service.deleteAllTweets();
	}
	
	
	//testing Create Tweet
	
	@Test
	  public void testCreateTweet () throws Exception
	  {
	    assertEquals (tweetArray.length, tweetList.size());
	    System.out.println(tweetList);
	    for (int i=1; i<tweetArray.length; i++)
	    {
	      assertEquals(tweetList.get(i), tweetArray[i]);
	    }
	  }
	
	
	// testing Delete Tweet
	
	@Test
	public void testDeleteTweet() throws Exception 
	{
		int code = service.deleteTweet(tweetArray[0].id);
		assertEquals(200, code);
	}

	
	// testing Get All Tweets
	
	@Test
	public void testGetAllTweets() throws Exception 
	{
		List<Tweet> list = service.getAllTweets();
		// and assert size list correct.
		assertEquals(list.size(), tweetList.size() - 1);
	}

	
	// testing Get Tweet
	
	@Test
	public void testGetTweet() throws Exception 
	{
		int code = service.getTweet(tweetArray[1].id);
		assertEquals(200, code);
	}
	
}

