package app.models;

import java.util.Date;
import java.util.UUID;

import com.google.common.base.Objects;

public class Tweet 
{

	public String id;
	public String tweetText;
	public String date;

	public Tweet() 
	{
	}

	public Tweet(String tweetText) 
	{
		this.id = UUID.randomUUID().toString();
		this.tweetText = tweetText;
		this.date = new Date().toString();
	}

	@Override
	// don't have to do this - John just double checking
	// - compares tweet echoed back with tweet generated
	public boolean equals(final Object obj) 
	{
		if (obj instanceof Tweet) 
		{
			final Tweet other = (Tweet) obj;
			return Objects.equal(id, other.id) && Objects.equal(tweetText, other.tweetText);
			// removiing date because of potential issue with seconds
			// && Objects.equal(date, other.date);
		} 
		else 
		{
			return false;
		}
	}
}
