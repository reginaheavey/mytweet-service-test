package app.main;

import java.util.List;

import app.models.Tweet;
import app.models.Tweeter;
import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;

public interface MyTweetsServiceProxy
{
	
	@POST("/api/tweeters")
	Call<Tweeter> createTweeter(@Path("id") String id, @Body String firstName, @Body String lastName, @Body String email, @Body String password);
	
  @POST("/api/tweeters/{id}/tweets")
  Call<Tweet> createTweet(@Path("id") String id, @Body Tweet tweet);

  @DELETE("/api/tweets/{id}")
  Call<Tweet> deleteTweet(@Path("id") String id);
  
  @GET("/api/tweets")
  Call<List<Tweet>> getAllTweets();
  
  @DELETE("/api/tweets")
  Call<String> deleteAllTweets();
  
  @GET("/api/tweets/{id}")
  Call<Tweet> getTweet(@Path("id") String id); 
  
}
