package app.main;

import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import app.models.Tweet;
import retrofit.Call;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

public class TweetAPI 
{
	private String service_url = "http://localhost:9000";
	// This IP address works for the test app but not for the Android app if
	// running within Genymotion or Android emulators. Following is a set of
	// service_urls for various target devices and a typical Heroku deployment:
	//
	// public String service_url = "http://10.0.2.2:9000"; //Android Emulator
	// public String service_url = "http://10.0.3.2:9000"; //Genymotion
	// public String service_url =
	// "https://mytweet-service-reginaheavey.herokuapp.com/";

	private MyTweetsServiceProxy service;

	public TweetAPI() 
	{
		Gson gson = new GsonBuilder().create();

		Retrofit retrofit = new Retrofit.Builder().baseUrl(service_url)
				.addConverterFactory(GsonConverterFactory.create(gson)).build();
		service = retrofit.create(MyTweetsServiceProxy.class);
	}

	public Tweet createTweet(String id, Tweet newTweet) throws Exception 
	{
		Call<Tweet> call = (Call<Tweet>) service.createTweet(id, newTweet);

		Response<Tweet> returnedTweet = call.execute();
	    return returnedTweet.body();
	}
	
	public int deleteTweet(String id) throws Exception 
	{
		Call<Tweet> call = service.deleteTweet(id);
		Response<Tweet> val = call.execute();
		return val.code();
	}

	public List<Tweet> getAllTweets() throws Exception 
	{
		Call<List<Tweet>> call = (Call<List<Tweet>>) service.getAllTweets();
		Response<List<Tweet>> tweets = call.execute();
		return tweets.body();
	}

	public int deleteAllTweets() throws Exception 
	{
		Call<String> call = service.deleteAllTweets();
		Response<String> val = call.execute();
		return val.code();
	}

	// public Tweet getTweet(String id) throws Exception
	public int getTweet(String id) throws Exception 
	{
		Call<Tweet> call = (Call<Tweet>) service.getTweet(id);
		Response<Tweet> val = call.execute();
		return val.code();
	}

}